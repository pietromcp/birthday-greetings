using System.Collections.Generic;

namespace BirthdayGreetings {
    public interface EmployeeRepository {
        IEnumerable<Employee> GetAllEmployees();
    }
}