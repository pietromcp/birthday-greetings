namespace BirthdayGreetings {
    public interface GreetingsNotifier {
        void Notify(Employee employee);
    }
}