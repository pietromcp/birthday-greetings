using System;

namespace BirthdayGreetings {
    public class BirthdayService {
        private readonly EmployeeRepository repo;
        private readonly GreetingsNotifier notifier;

        public BirthdayService(EmployeeRepository repo, GreetingsNotifier notifier) {
            this.repo = repo;
            this.notifier = notifier;
        }

        public void SendGreetings(DateTime today) {
            var employees = repo.GetAllEmployees();
            foreach (var employee in employees) {
                if (EmployeeHasBirthdayToday(today, employee)) {
                    notifier.Notify(employee);
                }
            }
        }

        private static bool EmployeeHasBirthdayToday(DateTime today, Employee employee) {
            return today.Date.Month == employee.Birthday.Date.Month && today.Date.Day == employee.Birthday.Date.Day;
        }
    }
}