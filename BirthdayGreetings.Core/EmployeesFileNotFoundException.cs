using System;

namespace BirthdayGreetings {
    public class EmployeesFileNotFoundException : Exception {
        public EmployeesFileNotFoundException(string fileName) : base($"Employees file name {fileName} not found") {
        }
    }
}