using System;

namespace BirthdayGreetings {
    public class Employee {
        public Employee(string firstName, string lastName, DateTime birthday, string email) {
            FirstName = firstName;
            LastName = lastName;
            Birthday = birthday;
            Email = email;
        }

        public string FirstName { get; }
        public string LastName { get;  }
        public string Email { get;  }
        public DateTime Birthday { get;  }
    }
}