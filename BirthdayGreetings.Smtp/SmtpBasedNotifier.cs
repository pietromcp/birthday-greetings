using System.Net.Mail;

namespace BirthdayGreetings {
    public class SmtpBasedNotifier : GreetingsNotifier {
        private readonly string hostname;
        private readonly int port;

        public SmtpBasedNotifier(string hostname, int port) {
            this.hostname = hostname;
            this.port = port;
        }

        public void Notify(Employee employee) {
            MailMessage message = new MailMessage {
                Subject = "Happy birthday!",
                Body = $"Happy birthday, dear {employee.FirstName}!",
                From = new MailAddress("system@example.com"),
                IsBodyHtml = false
            };
            message.To.Add(new MailAddress(employee.Email));
            
            SmtpClient client = new SmtpClient {
                Port = port,
                Host = hostname,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                EnableSsl = false
            };

            client.Send(message);
        }
    }
}