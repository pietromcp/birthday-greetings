using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace BirthdayGreetings {
    public class FileBasedEployeeRepository : EmployeeRepository {
        private readonly string fileName;

        public FileBasedEployeeRepository(string fileName) {
            this.fileName = fileName;
        }

        public IEnumerable<Employee> GetAllEmployees() {
            EnsureFileExists();

            var rows = File.ReadAllLines(fileName);
            return rows.Skip(1).Select(BuildEmployee);
        }

        private void EnsureFileExists() {
            if (!File.Exists(fileName)) {
                throw new EmployeesFileNotFoundException(fileName);
            }
        }

        private Employee BuildEmployee(string row) {
            var fields = row.Split(',');
            return new Employee(GetStringField(fields, 1),
                GetStringField(fields, 0), 
                GetDateTimeField(fields, 2),
                GetStringField(fields, 3));
        }

        private string GetStringField(string[] fields, int index) => fields[index].Trim();

        private DateTime GetDateTimeField(string[] fields, int index) {
            var dateAsText = GetStringField(fields, index);
            return DateTime.ParseExact(dateAsText, "yyyy/MM/dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
        }
    }
}