using NUnit.Framework;
using Xunit;
using Assert = NUnit.Framework.Assert;

namespace BirthdayGreetings.Tests {
    public class GreeterTest {
        [Fact]
        public void SayHelloWorld() {
            Assert.That(new Greeter("Hello").SayHello(), Is.EqualTo("Hello, World!"));
        }

        [Fact]
        public void SayHelloToSomeone() {
            Assert.That(new Greeter().SayHelloTo("Pietro"), Is.EqualTo("Hello, Pietro!"));
        }

        [Fact]
        public void UseCustomPhrase() {
            Assert.That(new Greeter("Hola").SayHelloTo("Pietro"), Is.EqualTo("Hola, Pietro!"));
            
        }
    }
}