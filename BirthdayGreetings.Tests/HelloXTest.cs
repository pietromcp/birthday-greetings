using Xunit;

namespace BirthdayGreetings.Tests {
    public class HelloXTest {
        [Fact]
        public void SayHelloToDefaultTarget() {
            Assert.Equal("Hello, World!", new Hello().SayHello());
        }
        
        [Fact]
        public void SayHelloToSomeone() {
            Assert.Equal("Hello, Pietro!", new Hello().SayHelloTo("Pietro"));
        }
    }
}