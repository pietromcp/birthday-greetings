using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Xunit;
using Assert = NUnit.Framework.Assert;

namespace BirthdayGreetings.Tests {
    public class FileBasedEployeeRepositoryTest {
        private readonly string fileName;
        private readonly EmployeeRepository repo;

        public FileBasedEployeeRepositoryTest() {
            fileName = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            repo = new FileBasedEployeeRepository(fileName);
        }

        [Fact]
        public void FileBasedEployeeRepository_GetEmployees_HappyPath() {
            PrepareHappyPathInputFile();
            var employees = repo.GetAllEmployees();
            Assert.That(employees.Count(), Is.EqualTo(3));
        }

        [Fact]
        public void FileBasedEployeeRepository_GetEmployees_FirstNames() {
            PrepareHappyPathInputFile();
            var employees = repo.GetAllEmployees();
            Assert.That(employees.Select(e => e.FirstName), Is.EquivalentTo(new[] {"John", "Mary", "Pietro"}));
        }
        
        [Fact]
        public void FileBasedEployeeRepository_GetEmployees_PietroMartinelli() {
            PrepareHappyPathInputFile();
            var employees = repo.GetAllEmployees();
            Assert.That(employees.Any(e => e.FirstName == "Pietro" && e.LastName == "Martinelli" && e.Email == "pietro.martinelli@foobar.com" && e.Birthday == new DateTime(1978, 3, 19, 0, 0, 0, DateTimeKind.Unspecified)));
        }

        [Fact]
        public void FileBasedEployeeRepository_GetEmployees_UnexistingInputFile() {
            var repo = new FileBasedEployeeRepository(Path.Combine(Path.GetTempFileName(), Path.GetRandomFileName()));
            Assert.Throws<EmployeesFileNotFoundException>(() => repo.GetAllEmployees());
        }

        private void PrepareHappyPathInputFile() {
            File.WriteAllText(fileName, @"last_name, first_name, date_of_birth, email
Doe, John, 1982/10/08, john.doe@foobar.com
Ann, Mary, 1975/09/11, mary.ann@foobar.com
Martinelli, Pietro, 1978/03/19, pietro.martinelli@foobar.com");
        }
    }
}