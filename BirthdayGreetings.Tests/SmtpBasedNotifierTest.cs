using System;
using NUnit.Framework;
using Xunit;
using Assert = NUnit.Framework.Assert;

namespace BirthdayGreetings.Tests {
    [Collection("Smtp collection")]
    public class SmtpBasedNotifierTest : IDisposable {
        private readonly SmtpFixture fixture;
        private readonly GreetingsNotifier notifier;
        
        
        public SmtpBasedNotifierTest(SmtpFixture fixture) {
            this.fixture = fixture;
            this.notifier = new SmtpBasedNotifier("localhost", SmtpFixture.Port);
        }

        [Fact]
        public void ShouldSendEmailMessage() {
            notifier.Notify(new Employee("Pietro", "Martinelli", DateTime.Now, "pietro@example.com"));
            
            Assert.That(fixture.Server.ReceivedEmailCount, Is.EqualTo(1));
        }
        
        [Fact]
        public void SentMessageShouldHaveExpectedFrom() {
            notifier.Notify(new Employee("Pietro", "Martinelli", DateTime.Now, "pietro@example.com"));
            
            var received = fixture.Server.ReceivedEmail[0];
            
            Assert.That(received.FromAddress.Address, Is.EqualTo("system@example.com"));
        }
        
        [Fact]
        public void SentMessageShouldHaveExpectedBody() {
            notifier.Notify(new Employee("Pietro", "Martinelli", DateTime.Now, "pietro@example.com"));
            
            var received = fixture.Server.ReceivedEmail[0];
            
            Assert.That(received.MessageParts[0].BodyData, Is.EqualTo("Happy birthday, dear Pietro!"));
        }
        
        [Fact]
        public void SentMessageShouldHaveExpectedSubject() {
            notifier.Notify(new Employee("Pietro", "Martinelli", DateTime.Now, "pietro@example.com"));
            
            var received = fixture.Server.ReceivedEmail[0];
            
            Assert.That(received.Headers["Subject"], Is.EqualTo("Happy birthday!"));
        }

        public void Dispose() {
            fixture.Server.ClearReceivedEmail();
        }
    }
}