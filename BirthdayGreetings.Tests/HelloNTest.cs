﻿using NUnit.Framework;

namespace BirthdayGreetings.Tests {
    [TestFixture]
    public class HelloNTest {
        [Test]
        public void SayHelloToDefaultTarget() {
            Assert.That(new Hello().SayHello(), Is.EqualTo("Hello, World!"));
        }
        
        [Test]
        public void SayHelloToSomeone() {
            Assert.That(new Hello().SayHelloTo("Pietro"), Is.EqualTo("Hello, Pietro!"));
        }
    }
}