using System;
using netDumbster.smtp;

namespace BirthdayGreetings.Tests {
    public class SmtpFixture : IDisposable {
        public const int Port = 10000;
        
        public SmtpFixture() {
            Server = SimpleSmtpServer.Start(Port);
        }

        public SimpleSmtpServer Server { get; }


        public void Dispose() {
            Server?.Dispose();
        }
    }
}