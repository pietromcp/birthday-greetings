using System;
using Moq;
using Xunit;

namespace BirthdayGreetings.Tests {
    public class BirthdayServiceTest {
        private readonly Mock<EmployeeRepository> repo;
        private readonly Mock<GreetingsNotifier> notifier;
        private readonly BirthdayService service;

        public BirthdayServiceTest() {
            repo = new Mock<EmployeeRepository>();
            notifier = new Mock<GreetingsNotifier>();
            service = new BirthdayService(repo.Object, notifier.Object);
        }

        [Fact]
        public void SingleEmployeeWithoutBirthday() {
            repo.Setup(r => r.GetAllEmployees()).Returns(new[ ]{new Employee("Pietro", "Martinelli", new DateTime(1978, 3, 19), "pietro@foobar.com")});
            service.SendGreetings(new DateTime(2020, 5, 11));
            notifier.Verify(n => n.Notify(It.IsAny<Employee>()), Times.Never());
        }

        [Fact]
        public void SingleEmployeeHavingBirthdayToday() {
            repo.Setup(r => r.GetAllEmployees()).Returns(new[ ]{new Employee("Pietro", "Martinelli", new DateTime(1978, 3, 19), "pietro@foobar.com")});
            service.SendGreetings(new DateTime(2020, 3, 19));
            notifier.Verify(n => n.Notify(It.Is<Employee>(e => e.FirstName == "Pietro")), Times.Once);
        }
        
        [Fact]
        public void ShouldNotifyOnlyEmployeesHavingBirthdayToday() {
            repo.Setup(r => r.GetAllEmployees()).Returns(new[ ] {
                new Employee("Pietro", "Martinelli", new DateTime(1978, 3, 19), "pietro@foobar.com"),
                new Employee("Paolo", "Martinelli", new DateTime(1980, 3, 19), "paolo@foobar.com"),
                new Employee("Giovanni", "Martinelli", new DateTime(1985, 5, 5), "giovanni@foobar.com")
            });
            service.SendGreetings(new DateTime(2020, 3, 19));
            notifier.Verify(n => n.Notify(It.IsAny<Employee>()), Times.Exactly(2));
        }
    }
}