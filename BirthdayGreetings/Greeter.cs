namespace BirthdayGreetings {
    public class Greeter {
        private readonly string phrase;

        public Greeter(string phrase) {
            this.phrase = phrase;
        }

        public Greeter() : this("Hello") {
        }

        public string SayHello() {
            return SayHelloTo("World");
        }

        public string SayHelloTo(string to) {
            return $"{phrase}, {to}!";
        }
    }
}