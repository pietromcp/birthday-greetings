﻿namespace BirthdayGreetings {
    public class Hello {
        public string SayHello() {
            return SayHelloTo("World");
        }

        public string SayHelloTo(string to) {
            return $"Hello, {to}!";
        }
    }
}