using System;
using System.IO;

namespace BirthdayGreetings {
    public class Program {
        public static void Main() {
            var repo = new FileBasedEployeeRepository(Path.Combine(Environment.CurrentDirectory, "employees.data.txt"));
            var notifier = new SmtpBasedNotifier("localhost", 25);
            var service = new BirthdayService(repo, notifier);
            service.SendGreetings(new DateTime(2021, 1, 1));
        }
    }
}